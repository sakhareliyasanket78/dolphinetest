<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <style type="text/css">
        .wrapper {
            width: 650px;
            margin: 0 auto;
        }

        .page-header h2 {
            margin-top: 0;
        }

        table tr td:last-child a {
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row ">

        <!-- The Modal for delete -->
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form action="" method="post" id="deleteform">
                            <div class="alert alert-danger">
                                <input type="hidden" name="deleteid" class="deleteid" value=""/>
                                <p>Are you sure you want to delete this record?</p><br>
                                <p>
                                    <input type="submit" value="Yes" class="btn btn-danger">
                                    <a href="index.php" class="btn btn-default">No</a>
                                </p>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <?php
        // Include config file
        require_once "config.php";

        // Attempt select query execution
        $sql = "SELECT * FROM products";
        if ($result = mysqli_query($link, $sql)) {
            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_array($result)) {
                    echo "<div class='row'>";
                    echo "<div class='col-md-3 mx-2'>";
                    echo "<div class='card' style='width: 18rem;'>";
                    echo "<div class='card-img-top' src='' alt='product image'>";
                    echo "<div class='card-body'>";
                    echo "<h5 class='card-title'>" . $row['product_name'] . "</h5>";
                    echo "<p class='card-text'>" . $row['ProductPrice'] . "</p>";
                    echo "<a href='update.php?id=" . $row['id'] . "' title='Update Record' class=' btn btn-primary'data-toggle='tooltip'>Update<span class='glyphicon glyphicon-pencil'></span></a>";
                    echo "<button   title='Delete Record'    id='delete'  class='delete btn btn-danger ml-3' value=" . $row['id'] . "  data-toggle='tooltip'>Delete</button>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";

                }
                mysqli_free_result($result);
            } else {
                echo "<p class='lead'><em>No records were found.</em></p>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
        }
        // Close connection
        mysqli_close($link);
        //                ?>

        
        <script>
            $('.delete').on('click', function () {
                var deleteid = $('#delete').val();
                $("#deleteid").val(deleteid);
                var action = 'delete.php?id=' + deleteid;
                $('#deleteform').attr('action', action);
                $('#myModal').show();
            });
        </script>


    </div>
</div>
</body>
</html>